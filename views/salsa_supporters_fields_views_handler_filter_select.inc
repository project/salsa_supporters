<?php

class salsa_supporters_fields_views_handler_filter_select extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $field = salsa_supporters_fields_load($this->definition['fid']);
      $field = array_pop($field);
      $field['extra'] = unserialize($field['extra']);
      $this->value_options = $field['extra']['form']['#options'];
      $this->value_options[''] = '<None>';
    }
  }
}
