<?php

/**
 * Implementation of hook_views_data().
 */
function salsa_supporters_fields_views_data() {
  $table = 'salsa_supporters_fields_data';
  $fields = salsa_supporters_fields_load('view', 'action');
  $table_aliases = array();

  foreach ($fields as &$field) {
    $field['extra'] = unserialize($field['extra']);
    $fname = $field['Database_Field'];
    $tname = $table . '_'. $fname;

    $table_aliases[$tname] = array(
      'table' => array(
        'group' => t('Salsa Supporters'),
        'join' => array(
          'users' => array(
            'left_field' => 'uid',
            'field' => 'uid',
            'table' => 'salsa_supporters_fields_data',
            'type' => 'LEFT',
            'extra' => array(
              array(
                'field' => 'fid',
                'operator' => '=',
                'value' => $field['fid'],
                'numeric' => TRUE,
              ),
            ),
          ),
        ),
      ),
    );
    $table_aliases[$tname][$fname] = array(
      'real field' => 'data',
      'title' => t($field['Display_Name']),
      'help' => t('Field drawn from Salsa.'),
      'field' => array(
        'handler' => 'salsa_supporters_fields_views_handler_field',
        'fid' => $field['fid'],
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    );

    $filter = NULL;
    switch ($field['salsa_type']) {
    case 'varchar':
      if ($field['extra']['form']['#type'] == 'select')
        $filter = 'salsa_supporters_fields_views_handler_filter_select';
      else 
        $filter = 'views_handler_filter_string';
      break;
    }

    if ($filter) {
      $table_aliases[$tname][$fname]['filter'] = array(
        'handler' => $filter,
        'fid' => $field['fid'],
      );
    }
  }

  return $table_aliases;
}

/**
 * Implementation of hook_views_handlers().
 */
function salsa_supporters_fields_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'salsa_supporters_fields') .'/views',
    ),
    'handlers' => array(
      'salsa_supporters_fields_views_handler_field' => array(
        'parent' => 'views_handler_field',
      ),
      'salsa_supporters_fields_views_handler_filter_select' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
    ),
  );
}
