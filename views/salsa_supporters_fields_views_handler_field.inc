<?php

class salsa_supporters_fields_views_handler_field extends views_handler_field {

  function init(&$view, $options) {
    parent::init($view, $options);
    $this->dbg = array(
      'options' => $options,
    );
  }

  function render($values) {
    //return '<xmp>'. print_r(array('vals' => $values, 'handler' => $this), true) .'</xmp>';
    $fid = $this->definition['fid'];
    if (salsa_supporters_fields_user_access($values->uid, $this->definition['fid'])) {
      return parent::render($values);
    }
    else {
      return NULL;
    }
  }

}