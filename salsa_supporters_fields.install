<?php

/**
 * Implementation of hook_install().
 */
function salsa_supporters_fields_install() {
  module_load_include('inc', 'salsa_supporters_fields', 'salsa_supporters_fields_api');
  module_load_include('inc', 'salsa_supporters_fields', 'salsa_supporters_fields_types');
  drupal_install_schema('salsa_supporters_fields');
  register_shutdown_function('drupal_get_schema', NULL, TRUE);
  register_shutdown_function('salsa_supporters_fields_install_shutdown');
  db_query("UPDATE {system} SET weight = 1 WHERE name = 'salsa_supporters_fields'");
}

/**
 * Shutdown helper function for hook_install().
 */
function salsa_supporters_fields_install_shutdown() {
  drupal_get_schema(NULL, TRUE);
  // Core Fields
  $fields = salsa_supporters_fields_types();
  foreach ($fields as $name => $field) {
    $type = $field['type'];
    unset($field['type']);
    $record = array(
      'Database_Field' => $name, 
      'Display_Name' => salsa_supporters_fields_display_name($name),
      'custom_field_KEY' => 0,
      'salsa_table' => 'supporter',
      'salsa_type' => $type,
      'extra' => serialize($field),
      'weight' => 0,
    );
    if (strpos($name, '_KEY') !== FALSE) {
      $record['sync'] = 1;
    }
    drupal_write_record('salsa_supporters_fields', $record);
  }
  
  // Custom Fields & Data (if API enabled)
  if (variable_get('salsa_api_url', NULL) && variable_get('salsa_api_username', NULL) && variable_get('salsa_api_password', NULL)) {
    salsa_supporters_fields_custom_refresh();
    salsa_supporters_fields_data_refresh();
  }
}

/**
 * Implementation of hook_uninstall().
 */
function salsa_supporters_fields_uninstall() {
  drupal_uninstall_schema('salsa_supporters_fields');
}

/**
 * Implementation of hook_schema().
 */
function salsa_supporters_fields_schema() {
  $schema = array();

  $schema['salsa_supporters_fields'] = array(
    'description' => t('Type and Storage Metadata for Mirrored Salsa Fields'),
    'fields' => array(
      'fid' => array(
        'description' => t('Primary key for this field meta-data'),
        'type' => 'serial',
      ),
      'Database_Field' => array(
        'description' => t('Salsa internal field name'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 64,
      ),
      'Display_Name' => array(
        'description' => t('Salsa human-readable field label'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 128,
      ),
      'custom_field_KEY' => array(
        'description' => t('Salsa unique id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'salsa_table' => array(
        'description' => t('Which Salsa table this field belongs to'),
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => 'supporter',
        'length' => 64,
      ),
      'salsa_type' => array(
        'description' => t('Field type for validation purposes'),
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => 'varchar',
        'length' => 16,
      ),
      'sync' => array(
        'description' => t('Determines whether this field gets synced from Salsa'),
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'view' => array(
        'description' => t('Determines whether this field is viewable on a user profile'),
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'edit' => array(
        'description' => t('Determines whether this field is editable on the user edit form'),
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'register' => array(
        'description' => t('Determines whether this field is present on the user registration form'),
        'type' => 'int',
        'size'=> 'tiny',
        'default' => 0,
      ),
      'required' => array(
        'description' => t('Determines whether this field is required for an advanced profile'),
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
      ),
      'weight' => array(
        'description' => t('Order for this field to appear in user forms'),
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => FALSE,
        'default' => 0,
      ),
      'extra' => array(
        'description' => t('Misc metadata'),
        'type' => 'text',
      ),
    ),
    'primary key' => array('fid'),
  );

  $schema['salsa_supporters_fields_data'] = array(
    'description' => t('Local Copies of Salsa Supporter fields.'),
    'fields' => array(
      'ssfid' => array(
        'description' => t('Primary key for this field data.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => t('Drupal user id'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'fid' => array(
        'description' => 'Internal Salsa field id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'data' => array(
        'description' => t('Local copy of supporter data for this field'),
        'type' => 'text',
      ),
    ),
    'primary key' => array('ssfid'),
  );

  return $schema;

}