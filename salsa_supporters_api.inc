<?php

/**
 * Load supporter key from db.
 */
function salsa_supporters_load($id = NULL, $mode = 'uid', $sync = TRUE, $refresh = FALSE) {
  static $supporters = array();

  if ($id) {
    if ($mode == 'uid') {
      if (isset($supporters[$id]) && !$refresh) {
	return $supporters[$id];
      } 
      else {
	$where = ' WHERE uid = %d ';
      }
    }
    else if ($mode == 'supporter_KEY') {
      $key = array_search($id, $supporters);
      if ($key !== FALSE && !$refresh) {
	return $key;
      } 
      else {
	$where = ' WHERE supporter_KEY = %d ';
      }
    }
  } 
  else {
    $where = ' WHERE 1=1 ';
  }
  
  if ($sync) {
    $where .= ' AND sync_enabled = 1 ';
  }
  
  $res = db_query("SELECT uid, supporter_KEY, sync_enabled FROM {salsa_supporters} $where", $id);

  if ($id) {
    $row = db_fetch_array($res);
    $supporters[$row['uid']] = $row; 
    return $supporters[$row['uid']];
  }
  else {
    $supporter_set = array();
    while ($row = db_fetch_array($res)) {
      $supporters[$row['uid']] = $supporter_set[$row['uid']] = $row;
    }
  }
  return $supporter_set;
}


/**
 * Fetch supporter information from Salsa.
 */
function salsa_supporters_fetch($mail) {
  $query = array(
    '#script' => 'getObjects.sjs',
    '#tables' => 'supporter',
    '#include' => 'Email',
    '#condition' => 'Email='. $mail,
  );

  $supporter = salsa_api_query($query);
  $supporter = salsa_supporters_fix($supporter['supporter']);
  return $supporter[0];
}


/**
 * Save supporter information to local db.
 */
function salsa_supporters_save($uid, $supporter) {
  
  if (isset($supporter['key'])) {
    $record = array(
      'uid' => $uid, 
      'supporter_KEY' => $supporter['key'], 
    );
    if (isset($supporter['sync_enabled'])) {
      $record['sync_enabled'] = $supporter['sync_enabled'];
    }
    
    drupal_write_record('salsa_supporters', $record, array('uid'));
    
    if (db_affected_rows() == 0) {
      drupal_write_record('salsa_supporters', $record);
    }
  }
  else {
    db_query("DELETE FROM {salsa_supporters} WHERE uid = %d", $uid);
  }
}

/**
 * Create a supporter in Salsa.
 */
function salsa_supporters_push($supporter) {
  $query = array(
    '#script' => 'save',
    '#tables' => 'supporter',
    '#fields' => $supporter,
  );

  $supporter = salsa_api_query($query);
  $supporter = salsa_supporters_fix($supporter);
  return $supporter[0];
}

/**
 * Synchronize Supporter Keys.
 * Implementation of hook_salsa_sync().
 */
function salsa_supporters_salsa_sync() {
  $res = db_query("SELECT uid, mail FROM {users} WHERE uid > 0");

  $uids = array();
  while ($row = db_fetch_array($res)) {
    $uids[$row['uid']] = $row['mail'];
  }
  
  foreach ($uids as $uid => $mail) {
    $supporter = salsa_supporters_fetch($mail);
    salsa_supporters_save($uid, $supporter);
  }
}

/**
 * Helper function for handling parsed XML.
 */
function salsa_supporters_fix($data) {
    if(isset($data['count'])) {
      if ($data['count'] == 1) {
	return array($data['item']);
      } else {
	return $data['item'];
      }
    } else {
      $data_array = array(
	'#count' => 1,
	'#item' => $data,
      );
      return $data_array;
    }
}