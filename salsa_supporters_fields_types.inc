<?php

/**
 * Stores type information for supporter fields.
 * If the API ever allows this to be retrieved dynamically, we should replace this with that.
 * In the meantime, this will have to remain hard-coded.
 */
function salsa_supporters_fields_types() {
  
  return array(
    'supporter_KEY' => array(
      'type' => 'int',
    ),
    'organization_KEY' => array(
      'type' => 'int',
    ),
    'chapter_KEY' => array(
      'type' => 'int',
    ),                                                      
    'Last_Modified' => array(
      'type' => 'timestamp',
    ),
    'Date_Created' => array(
      'type' => 'timestamp',
    ),
    'Title' => array(
      'type' => 'varchar',
    ),
    'First_Name' => array(
      'type' => 'varchar',
    ),
    'MI' => array(
      'type' => 'varchar',
    ),
    'Last_Name' => array(
      'type' => 'varchar',
    ),
    'Suffix' => array(
      'type' => 'varchar',
    ),
    'Email' => array(
      'type' => 'varchar',
    ),
    'Password' => array(
      'type' => 'varchar',
    ),
    'Receive_Email' => array(
      'type' => 'tinyint',
    ),
    'Email_Status' => array(
      'type' => 'enum',
      'options' => array('N/A','Good','Soft Bounce','Hard Bounce'),
    ),
    'Email_Preference' => array(
      'type' => 'enum',
      'options' => array('HTML Email','Text Email'),
    ),
    'Soft_Bounce_Count' => array(
      'type' => 'int',
    ),                               
    'Hard_Bounce_Count' => array(
      'type' => 'int',
    ),
    'Last_Bounce' => array(
      'type' => 'datetime',
    ),                                                  
    'Receive_Phone_Blasts' => array(
      'type' => 'tinyint',
    ),
    'Phone' => array(
      'type' => 'varchar',
    ),
    'Cell_Phone' => array(
      'type' => 'varchar',
    ),
    'Phone_Provider' => array(
      'type' => 'varchar',
    ),
    'Work_Phone' => array(
      'type' => 'varchar',
    ),
    'Pager' => array(
      'type' => 'varchar',
    ),                                               
    'Home_Fax' => array(
      'type' => 'varchar',
    ),                                               
    'Work_Fax' => array(
      'type' => 'varchar',
    ),                                               
    'Street' => array(
      'type' => 'varchar',
    ),                                               
    'Street_2' => array(
      'type' => 'varchar',
    ),                                               
    'Street_3' => array(
      'type' => 'varchar',
    ),                                              
    'City' => array(
      'type' => 'varchar',
    ),                                               
    'State' => array(
      'type' => 'varchar',
      'form' => array(
        '#type' => 'select',
        '#options' => array(
          '' => '',
          'AL'=>"Alabama",
          'AK'=>"Alaska", 
          'AZ'=>"Arizona", 
          'AR'=>"Arkansas", 
          'CA'=>"California", 
          'CO'=>"Colorado", 
          'CT'=>"Connecticut", 
          'DE'=>"Delaware", 
          'DC'=>"District Of Columbia", 
          'FL'=>"Florida", 
          'GA'=>"Georgia", 
          'HI'=>"Hawaii", 
          'ID'=>"Idaho", 
          'IL'=>"Illinois", 
          'IN'=>"Indiana", 
          'IA'=>"Iowa", 
          'KS'=>"Kansas", 
          'KY'=>"Kentucky", 
          'LA'=>"Louisiana", 
          'ME'=>"Maine", 
          'MD'=>"Maryland", 
          'MA'=>"Massachusetts", 
          'MI'=>"Michigan", 
          'MN'=>"Minnesota", 
          'MS'=>"Mississippi", 
          'MO'=>"Missouri", 
          'MT'=>"Montana",
          'NE'=>"Nebraska",
          'NV'=>"Nevada",
          'NH'=>"New Hampshire",
          'NJ'=>"New Jersey",
          'NM'=>"New Mexico",
          'NY'=>"New York",
          'NC'=>"North Carolina",
          'ND'=>"North Dakota",
          'OH'=>"Ohio", 
          'OK'=>"Oklahoma", 
          'OR'=>"Oregon", 
          'PA'=>"Pennsylvania", 
          'RI'=>"Rhode Island", 
          'SC'=>"South Carolina", 
          'SD'=>"South Dakota",
          'TN'=>"Tennessee", 
          'TX'=>"Texas", 
          'UT'=>"Utah", 
          'VT'=>"Vermont", 
          'VA'=>"Virginia", 
          'WA'=>"Washington", 
          'WV'=>"West Virginia", 
          'WI'=>"Wisconsin", 
          'WY'=>"Wyoming"),
      ),
    ),                                               
    'Zip' => array(
      'type' => 'varchar',
    ),                                               
    'PRIVATE_Zip_Plus_4' => array(
      'type' => 'varchar',
    ),                                                
    'County' => array(
      'type' => 'varchar',
    ),                                               
    'District' => array(
      'type' => 'varchar',
    ),                                               
    'Country' => array(
      'type' => 'varchar',
    ),                                               
    'Latitude' => array(
      'type' => 'float',
    ),
    'Longitude' => array(
      'type' => 'float',
    ),
    'Organization' => array(
      'type' => 'varchar',
    ),                                              
    'Department' => array(
      'type' => 'varchar',
    ),                                              
    'Occupation' => array(
      'type' => 'varchar',
    ),    
    'Instant_Messenger_Service' => array(
      'type' => 'enum',
      'options' => array('AOL', 'Yahoo', 'MSN', 'ICQ', 'Other'),
    ),
    'Instant_Messenger_Name' => array(
      'type' => 'varchar',
    ),
    'Web_Page' => array(
      'type' => 'varchar',
    ),
    'Alternative_Email' => array(
      'type' => 'varchar',
    ),
    'Other_Data_1' => array(
      'type' => 'varchar',
    ),
    'Other_Data_2' => array(
      'type' => 'varchar',
    ),
    'Other_Data_3' => array(
      'type' => 'varchar',
    ),
    'Notes' => array(
      'type' => 'blob',
    ),
    'Source' => array(
      'type' => 'varchar',
    ),
    'Source_Details' => array(
      'type' => 'varchar',
    ),
    'Source_Tracking_Code' => array(
      'type' => 'varchar',
    ),
    'Tracking_Code' => array(
      'type' => 'varchar',
    ),
    'Status' => array(
      'type' => 'enum',
      'options' => array('Active', 'Temporarily Inactive', 'Invisible', 'Inactive'),
    ),
    'uid' => array(
      'type' => 'varchar',
    ),
    'Timezone' => array(
      'type' => 'varchar',
    ),
  );
}

function salsa_supporters_fields_display_name($field_name) {

  return str_replace('_', ' ', $field_name);

}