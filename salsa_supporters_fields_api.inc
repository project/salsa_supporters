<?php

function salsa_supporters_fields_actions() {
  return array('sync', 'view', 'edit', 'register', 'required');
}

/**
 * API Function: Load local fields
 * @return
 *  An array of the requested fields.
 */
function salsa_supporters_fields_load($id = NULL, $mode = 'fid') {
  static $fields = array();
  $where = '';
  if ($id) {
    switch ($mode) {
    case 'fid':
      if (isset($fields[$id])) {
	return array($id => $fields[$id]);
      }
      $where = ' fid = %d ';
      break;
    case 'salsa_table':
      $where = " salsa_table = '%s' ";
      break;
    case 'action':
      if (in_array($id, salsa_supporters_fields_actions())) {
	$where = " $id = 1 ";
      }
      break;
    }
    if (count($where)) {
      $where = ' WHERE ' . $where;
    }
  }

  $res = db_query("SELECT * FROM {salsa_supporters_fields} $where ORDER BY weight, fid ", $id);
 
  $field_set = array();
  while ($row = db_fetch_array($res)) {
    $field_set[$row['fid']] = array(
      'key' => $row['custom_field_KEY'],
      'object' => 'custom_field',
    );
    foreach ($row as $key => $value) {
      $field_set[$row['fid']][$key] = $value;
    }
    $fields[$row['fid']] = $field_set[$row['fid']];
  }

  return $field_set;
}

/**
 * API Function: Load field data for a user
 */
function salsa_supporters_fields_data_load($uid = NULL, $action = 'sync') {
  $where = array();
  $args = array();
  if ($uid) {
    $where[] = ' ssfd.uid = %d ';
    $args[] = $uid;
  }
  switch ($action) {
    case 'sync':
    case 'view':
    case 'edit':
    case 'required':
      $where[] = " ssf.$action = 1 ";
      break;
  }

  $res = db_query(
    "SELECT * FROM {salsa_supporters_fields_data} ssfd ".
    "LEFT JOIN {salsa_supporters_fields} ssf ON ssfd.fid = ssf.fid ".
    (count($where) ? "WHERE ". implode(" AND ", $where) : '') .' '.
    "ORDER BY uid, ssf.weight, ssf.fid ",
    $args
  );

  $data = array();
  while ($row = db_fetch_array($res)) {
    $data[] = $row;
  }

  return $data;
}

/**
 * API Function: Save field data for a user
 */
function salsa_supporters_fields_data_save($uid, $records) {
  db_query('DELETE FROM {salsa_supporters_fields_data} WHERE uid = %d', $uid);

  foreach ($records as $record) {
    $record['uid'] = $uid;
    drupal_write_record('salsa_supporters_fields_data', $record);
  }
}

/**
 * API Function: Push field data for a user back to Salsa
 */
function salsa_supporters_fields_data_push($supporter_KEY, $fields) {
  $supporter_fields = array();
  $supporter_custom_fields = array();

  foreach ($fields as $field) {
    if ($field['edit']) {
      if ($field['salsa_table'] == 'supporter') {
        $supporter_fields[$field['Database_Field']] = $field['data'];
      } 
      else if ($field['salsa_table'] == 'supporter_custom') {
        $supporter_custom_fields[$field['Database_Field']] = $field['data'];
      }
    }
  }

  $query = array(
    '#script' => 'save',
    '#tables' => 'supporter',
    '#fields' => array_merge(array('key' => $supporter_KEY), $supporter_fields),
  );

  $ret = salsa_api_query($query);

  $query = array(
    '#script' => 'save',
    '#tables' => 'supporter_custom',
    '#fields' => array_merge(array('supporter_KEY' => $supporter_KEY), $supporter_custom_fields),
  );
  $ret = salsa_api_query($query);
}

/**
 * API Function: Fetch field data for a user
 */
function salsa_supporters_fields_data_fetch($supporter_KEY, $table, $fields) {

  $query = array(
    '#script' => 'getObjects.sjs',
    '#tables' => $table,
    '#condition' => 'supporter_KEY='. $supporter_KEY,
  );
  $result = salsa_api_query($query);
  $result = salsa_supporters_fix($result[$table]);
  $result = $result[0];
  
  $records = array();
  foreach ($fields as $field) {
    if ($field['sync'] && $field['salsa_table'] == $table) {
      $name = $field['Database_Field'];
      $data = $result[$name];
      $record = array('fid' => $field['fid'], 'data' => $data);
      //drupal_write_record('salsa_supporters_fields_data', $record);
      $records[$field['fid']] = $record;
    }
  }

  return $records;
}

/**
 * Pull custom field listing from Salsa & save
 */
function salsa_supporters_fields_custom_refresh() {
  // Get list of Salsa fields
  $query = array(
    '#script' => 'getObjects.sjs',
    '#tables' => 'custom_field',
  );
  $result = salsa_api_query($query);


  $salsa_fields = salsa_supporters_fix($result['custom_field']);
  if (!count($salsa_fields)) {
    $salsa_fields = array();
  }
  $salsa_field_keys = array_map(create_function('$f', 'return $f["key"];'), $salsa_fields);

  // Get locally stored Salsa fields
  $local = salsa_supporters_fields_load('supporter_custom', 'salsa_table');
  $local_keys = array_map(create_function('$f', 'return $f["custom_field_KEY"];'), $local);
  
  if (count($local)) {
    $local = array_combine($local_keys, $local);
    foreach ($local as $field) {
      if (!in_array($field['custom_field_KEY'], $salsa_field_keys)) {
        // TODO: Local field has been deleted - remove local data and disable field
      }
    }
  }

  foreach ($salsa_fields as $field) {
    $update = array();
    $write_fields = array(
      'Database_Field' => $field['READONLY_Database_Field'],
      'Display_Name' => $field['Display_Name'],
      'custom_field_KEY' => $field['custom_field_KEY'],
      'salsa_table' => 'supporter_custom',
      'salsa_type' => str_replace(
        array('0','1','2','3','4','5','6','7','8','9'), 
        '', 
        strtolower($field['READONLY_Database_Field'])
      ),
    ); 

    if (in_array($field['custom_field_KEY'], $local_keys)) {
      $update = array('fid');
      $write_fields['fid'] = $local[$field['custom_field_KEY']]['fid'];
    }

    drupal_write_record('salsa_supporters_fields', $write_fields, $update);
  }
  
  return 'test';
}

/**
 * API Function: Fetch and Save field data for all users
 */
function salsa_supporters_fields_data_refresh() {
  $supporter_KEYs = salsa_supporters_load();

  $core_fields = salsa_supporters_fields_load('supporter', 'salsa_table');
  $custom_fields = salsa_supporters_fields_load('supporter_custom', 'salsa_table');

  foreach ($supporter_KEYs as $item) {
    $key = $item['supporter_KEY'];
    $core_data = salsa_supporters_fields_data_fetch($key, 'supporter', $core_fields);
    $custom_data = salsa_supporters_fields_data_fetch($key, 'supporter_custom', $custom_fields);
    salsa_supporters_fields_data_save($item['uid'], array_merge($core_data, $custom_data));
  }

  return 'foo';
}


function salsa_supporters_fields_find_tables($fields) {
  $tables = array();
  foreach ($fields as $field) {
    $tables[] = $field['salsa_table'];
  }
  return array_unique($tables);
}


function salsa_supporters_fields_data_sort($field_data_list, $key) {
  $data = array();
  foreach ($field_data_list as $field_data) {
    $data[$field_data[$key]] = $field_data;
  }
  return $data;
}


